use anyhow::Result;
use reqwest::multipart::Form;
use reqwest::Client;

pub async fn paste(data: String) -> Result<String> {
    let form = Form::new().text("clbin", data);
    let client = Client::new();
    let res = client
        .post("https://clbin.com/")
        .multipart(form)
        .send()
        .await?;
    let text = res.text().await?;
    let url = text.lines().next().unwrap_or("").to_owned();
    Ok(url)
}
