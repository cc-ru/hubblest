use anyhow::{anyhow, Result};
use reqwest::Client;

pub async fn shorten(url: &str) -> Result<String> {
    let res = Client::new()
        .get("https://da.gd/s")
        .query(&[("url", url)])
        .send()
        .await?;
    let text = res.text().await?;
    let url = text.lines().next().ok_or(anyhow!("da.gd is broken"))?;
    Ok(url.to_owned())
}

pub async fn shorten_all(urls: impl Iterator<Item = String>) -> Result<Vec<String>> {
    let tasks = urls.map(|url| async move { shorten(&url).await });
    let results = futures::future::join_all(tasks).await;
    let result: Result<Vec<_>> = results.into_iter().collect();
    Ok(result?)
}
