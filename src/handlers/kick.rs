use std::time::Duration;

use anyhow::Result;
use tokio::time::delay_for;

use crate::bot::{BotContext, Handler};
use crate::irc::Message;

struct KickHandler;

#[async_trait::async_trait]
impl Handler for KickHandler {
    fn clone(&self) -> Box<dyn Handler> {
        Box::new(KickHandler)
    }

    fn can_handle(&self, bot: &mut BotContext, msg: &Message) -> bool {
        msg.command == "KICK" && msg.params.get(1) == Some(&bot.config.irc.nickname)
    }

    async fn handle(&mut self, bot: &mut BotContext, msg: Message) -> Result<()> {
        delay_for(Duration::from_secs(1)).await;
        bot.irc.join(&msg.params[0]).await?;
        Ok(())
    }
}

inventory::submit!({
    let handler: Box<dyn Handler> = Box::new(KickHandler);
    handler
});
