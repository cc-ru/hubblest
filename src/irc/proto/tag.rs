use std::fmt::{self, Display};
use std::str::FromStr;

use super::ParseError;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Tag {
    pub is_client_only: bool,
    pub vendor: Option<String>,
    pub key: String,
    pub value: Option<String>,
}

impl Tag {
    pub fn new(key: impl Into<String>) -> Self {
        Self {
            is_client_only: false,
            vendor: None,
            key: key.into(),
            value: None,
        }
    }

    pub fn with_value(mut self, value: impl Into<String>) -> Self {
        self.value = Some(value.into());
        self
    }

    pub fn with_vendor(mut self, vendor: impl Into<String>) -> Self {
        self.vendor = Some(vendor.into());
        self
    }

    pub fn with_client_only(mut self, on: bool) -> Self {
        self.is_client_only = on;
        self
    }
}

impl Display for Tag {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.is_client_only {
            f.write_str("+")?;
        }

        if let Some(vendor) = &self.vendor {
            f.write_str(vendor)?;
            f.write_str("/")?;
        }

        f.write_str(&self.key)?;

        if let Some(value) = &self.value {
            f.write_str("=")?;
            escape_tag_value(value, f)?;
        }

        Ok(())
    }
}

impl FromStr for Tag {
    type Err = ParseError;

    fn from_str(mut s: &str) -> Result<Tag, ParseError> {
        let is_client_only = s.starts_with('+');
        if is_client_only {
            s = &s[1..];
        }

        let mut parts = s.splitn(2, '=');
        let key = parts.next().ok_or(ParseError)?;
        let mut key_parts = key.splitn(2, '/');
        let mut vendor = key_parts.next().map(String::from);
        let key = key_parts
            .next()
            .map(String::from)
            .or_else(|| vendor.take())
            .ok_or(ParseError)?;
        let value = parts.next().map(unescape_tag_value);

        Ok(Tag {
            is_client_only,
            vendor,
            key,
            value,
        })
    }
}

fn escape_tag_value(value: &str, mut writer: impl fmt::Write) -> fmt::Result {
    for char in value.chars() {
        let escaped = match char {
            ';' => ':',
            ' ' => 's',
            '\\' => '\\',
            '\r' => 'r',
            '\n' => 'n',
            _ => {
                writer.write_char(char)?;
                continue;
            }
        };
        writer.write_char('\\')?;
        writer.write_char(escaped)?;
    }

    Ok(())
}

fn unescape_tag_value(value: &str) -> String {
    let mut result = String::with_capacity(value.len());
    let mut is_escaped = false;
    for char in value.chars() {
        if is_escaped {
            is_escaped = false;
            result.push(match char {
                ':' => ';',
                's' => ' ',
                'r' => '\r',
                'n' => '\n',
                _ => char,
            });
        } else if char == '\\' {
            is_escaped = true;
            continue;
        } else {
            result.push(char);
        }
    }
    result
}
