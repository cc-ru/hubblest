use std::str::FromStr;

use anyhow::Result;
use bytes::BytesMut;
use native_tls::TlsConnector as NativeTlsConnector;
use tokio::io::{split, AsyncReadExt, AsyncWriteExt, ReadHalf, WriteHalf};
use tokio::net::TcpStream;
use tokio::sync::{broadcast, mpsc};
use tokio_either::Either;
use tokio_tls::{TlsConnector, TlsStream};

use super::proto::Message;

type Connection = Either<TcpStream, TlsStream<TcpStream>>;

async fn connect_raw(hostname: &str, port: u16) -> Result<Connection> {
    let stream = TcpStream::connect((hostname, port)).await?;
    Ok(Either::Left(stream))
}

async fn connect_tls(hostname: &str, port: u16) -> Result<Connection> {
    let stream = TcpStream::connect((hostname, port)).await?;
    let connector = TlsConnector::from(NativeTlsConnector::new()?);
    let tls = connector.connect(hostname, stream).await?;
    Ok(Either::Right(tls))
}

async fn connect(hostname: &str, port: u16, tls: bool) -> Result<Connection> {
    if tls {
        connect_tls(hostname, port).await
    } else {
        connect_raw(hostname, port).await
    }
}

async fn send_loop(
    mut rx: mpsc::Receiver<Message>,
    mut socket: WriteHalf<Connection>,
) -> Result<()> {
    while let Some(msg) = rx.recv().await {
        let mut line = msg.to_string();
        log::info!(">>> {}", &line);
        line.push_str("\r\n");
        socket.write_all(line.as_bytes()).await?;
    }

    Ok(())
}

async fn recv_loop(tx: broadcast::Sender<Message>, mut socket: ReadHalf<Connection>) -> Result<()> {
    let mut buf = BytesMut::with_capacity(8192);
    loop {
        let n = socket.read_buf(&mut buf).await?;
        if n == 0 {
            anyhow::bail!("disconnected");
        }

        while let Some((crlf, _)) = buf.windows(2).enumerate().find(|(_, w)| w == b"\r\n") {
            let mut line = buf.split_to(crlf + 2);
            line.truncate(line.len() - 2);

            let line = String::from_utf8_lossy(&line).to_owned();
            let message = match Message::from_str(&line) {
                Ok(v) => v,
                Err(_) => {
                    log::error!("<<< {}", line);
                    continue;
                }
            };

            log::info!("<<< {}", line);
            tx.send(message.clone())
                .expect("failed to send to broadcast queue");
        }
    }
}

pub struct Client {
    mpsc_tx: mpsc::Sender<Message>,
    brct_rx: broadcast::Receiver<Message>,
    brct_tx: broadcast::Sender<Message>,
}

impl Client {
    pub async fn connect(hostname: &str, port: u16, tls: bool) -> Result<Client> {
        let (sock_rx, sock_tx) = split(connect(hostname, port, tls).await?);
        let (brct_tx, brct_rx) = broadcast::channel(128);
        let (mpsc_tx, mpsc_rx) = mpsc::channel(128);

        let brct_tx1 = brct_tx.clone();
        tokio::spawn(async {
            if let Err(e) = recv_loop(brct_tx1, sock_rx).await {
                log::error!("Error in receive loop: {}", e);
            }
        });

        tokio::spawn(async {
            if let Err(e) = send_loop(mpsc_rx, sock_tx).await {
                log::error!("Error in send loop: {}", e);
            }
        });

        Ok(Client {
            mpsc_tx,
            brct_rx,
            brct_tx,
        })
    }

    pub async fn send(&mut self, msg: impl Into<Message>) -> Result<()> {
        Ok(self
            .mpsc_tx
            .send(msg.into())
            .await
            .expect("failed to send to mpsc queue"))
    }

    pub async fn raw_recv(&mut self) -> Result<Message> {
        Ok(self
            .brct_rx
            .recv()
            .await
            .expect("failed to send to mpsc queue"))
    }

    pub async fn recv(&mut self) -> Result<Message> {
        loop {
            let mut msg = self.raw_recv().await?;
            if msg.command == "PING" {
                msg.command = "PONG".into();
                self.send(msg).await?;
            } else {
                return Ok(msg);
            }
        }
    }

    pub async fn recv_filter(&mut self, pred: impl Fn(&Message) -> bool) -> Result<Message> {
        loop {
            let msg = self.recv().await?;
            if pred(&msg) {
                return Ok(msg);
            }
        }
    }

    pub async fn wait_response(&mut self, code: u16) -> Result<Message> {
        self.recv_filter(|msg| {
            msg.command
                .parse::<u16>()
                .map(|v| v == code)
                .unwrap_or(false)
        })
        .await
    }

    pub async fn recv_privmsg(&mut self) -> Result<Message> {
        loop {
            let msg = self.recv().await?;
            if msg.command == "PRIVMSG" {
                return Ok(msg);
            }
        }
    }

    pub async fn nick(&mut self, nick: impl Into<String>) -> Result<()> {
        self.send(Message::new("NICK").with_param(nick)).await
    }

    pub async fn user(
        &mut self,
        username: impl Into<String>,
        realname: impl Into<String>,
    ) -> Result<()> {
        self.send(
            Message::new("USER")
                .with_param(username)
                .with_param("0")
                .with_param("*")
                .with_param(realname),
        )
        .await
    }

    pub async fn join(&mut self, channel: impl Into<String>) -> Result<()> {
        self.send(Message::new("JOIN").with_param(channel)).await
    }

    pub async fn privmsg(
        &mut self,
        target: impl Into<String>,
        message: impl Into<String>,
    ) -> Result<()> {
        self.send(
            Message::new("PRIVMSG")
                .with_param(target)
                .with_param(message),
        )
        .await
    }

    pub fn handle(&self) -> Client {
        Client {
            mpsc_tx: self.mpsc_tx.clone(),
            brct_rx: self.brct_tx.subscribe(),
            brct_tx: self.brct_tx.clone(),
        }
    }

    pub async fn is_oper(
        &mut self,
        channel: impl Into<String>,
        nick: impl Into<String>,
    ) -> Result<bool> {
        let mut nick = nick.into();
        nick.insert(0, '@');
        self.send(Message::new("NAMES").with_param(channel)).await?;
        let res = self.wait_response(353).await?;
        let names = res.params.get(3);
        let yea = names.map(|v| v.contains(&nick)).unwrap_or(false);
        Ok(yea)
    }
}
