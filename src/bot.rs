use anyhow::Result;
use async_trait::async_trait;
use sqlx::pool::PoolConnection;
use sqlx::{PgConnection, Pool};

use crate::config::Config;
use crate::irc::{Client, Message};

pub struct Bot {
    pub config: Config,
    pub irc: Client,
    pub db: Pool<PgConnection>,
    handlers: Vec<Box<dyn Handler>>,
}

pub struct BotContext {
    pub config: Config,
    pub irc: Client,
    pub db: PoolConnection<PgConnection>,
    pub msg: Message,
}

inventory::collect!(Box<dyn Handler>);

impl Bot {
    pub fn new(config: Config, irc: Client, db: Pool<PgConnection>) -> Bot {
        Bot {
            config,
            irc,
            db,
            handlers: vec![],
        }
    }

    pub fn harvest_handlers(&mut self) {
        for handler in inventory::iter::<Box<dyn Handler>> {
            self.handlers.push(Handler::clone(&**handler));
        }
    }

    async fn handle(&mut self, msg: Message) -> Result<()> {
        let mut ctx = BotContext {
            config: self.config.clone(),
            irc: self.irc.handle(),
            db: self.db.acquire().await?,
            msg: msg.clone(),
        };

        for handler in &mut self.handlers {
            if handler.can_handle(&mut ctx, &msg) {
                let mut ctx = BotContext {
                    config: self.config.clone(),
                    irc: self.irc.handle(),
                    db: self.db.acquire().await?,
                    msg: msg.clone(),
                };

                let msg = msg.clone();
                let mut handler = handler.clone();
                tokio::task::spawn(async move {
                    if let Err(e) = handler.handle(&mut ctx, msg).await {
                        log::error!("Handler error: {:?}", e);
                    }
                });
            }
        }
        Ok(())
    }

    pub async fn run(&mut self) -> Result<()> {
        loop {
            let msg = self.irc.recv().await?;
            self.handle(msg).await?;
        }
    }
}

#[async_trait]
pub trait Handler: Send {
    fn clone(&self) -> Box<dyn Handler>;

    fn can_handle(&self, _bot: &mut BotContext, _msg: &Message) -> bool {
        true
    }

    async fn handle(&mut self, bot: &mut BotContext, msg: Message) -> Result<()>;
}
