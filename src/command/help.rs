use anyhow::Result;

use crate::bot::BotContext;

async fn help(bot: &mut BotContext, _: String, target: String) -> Result<()> {
    bot.irc
        .privmsg(
            target,
            "https://gist.github.com/LeshaInc/df2e6c9f8408e11e8a1cc5a7425fe638",
        )
        .await
}

command!("help", help);
