use anyhow::Result;

use crate::bot::BotContext;
use crate::query::{self, Expr};
use crate::util;

const BOTS: &[&str] = &[
    "brote",
    "hubblest",
    "hubblest2",
    "yuki`",
    "yumi`",
    "ayumi`",
    "yui`",
    "fs24_bot",
    "zsh",
    "noobot",
];

async fn search(bot: &mut BotContext, command: String, target: String) -> Result<()> {
    let bots = BOTS.into_iter().map(|&s| s.to_owned()).collect::<Vec<_>>();
    let expr = Expr::parse(&command)?;
    let res = query::search(&mut bot.db, bots.clone(), expr).await?;
    util::send_search_result(&mut bot.irc, &target, res).await?;

    Ok(())
}

command!("search", search);

async fn top(bot: &mut BotContext, command: String, target: String) -> Result<()> {
    let bots = BOTS.into_iter().map(|&s| s.to_owned()).collect::<Vec<_>>();
    let expr = Expr::parse(&command)?;
    let res = query::top(&mut bot.db, bots.clone(), expr).await?;
    util::send_top_result(&mut bot.irc, &target, res).await?;

    Ok(())
}

command!("top", top);

async fn count(bot: &mut BotContext, command: String, target: String) -> Result<()> {
    let bots = BOTS.into_iter().map(|&s| s.to_owned()).collect::<Vec<_>>();
    let expr = Expr::parse(&command)?;
    let res = query::count(&mut bot.db, bots.clone(), expr).await?;
    util::send_count_result(&mut bot.irc, &target, res).await?;

    Ok(())
}

command!("count", count);
