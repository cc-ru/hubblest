use anyhow::{anyhow, bail, Result};

use crate::bot::BotContext;
use sqlx::prelude::*;

async fn add_alias(bot: &mut BotContext, cmd: String, target: String) -> Result<()> {
    let prefix = bot.msg.prefix.as_ref();
    let nickname = &prefix.ok_or(anyhow!("access denied"))?.origin;
    if !bot.irc.is_oper(&target, nickname).await? {
        bail!("access denied")
    }

    let mut parts = cmd.split_whitespace();
    let primary = parts
        .next()
        .ok_or(anyhow!("syntax: add-alias <primary> <secondary>..."))?;
    let secondary = parts.collect::<Vec<_>>();

    if secondary.is_empty() {
        bail!("syntax: add-alias <primary> <secondary>...");
    }

    sqlx::query(
        r#"INSERT INTO aliases (alias_primary, alias_secondary)
        SELECT $1 AS alias_primary, alias_secondary
        FROM unnest($2::text[]) AS query(alias_secondary)"#,
    )
    .bind(primary)
    .bind(secondary)
    .execute(&mut bot.db)
    .await?;

    bot.irc.privmsg(target, "Success.").await
}

command!("add-alias", add_alias);

async fn aliases(bot: &mut BotContext, cmd: String, target: String) -> Result<()> {
    if cmd.is_empty() || cmd.split_whitespace().count() > 1 {
        bail!("syntax: aliases <nickname>")
    }

    let nickname = cmd.trim().to_owned();

    let primary = sqlx::query_as("SELECT alias_primary FROM aliases WHERE alias_secondary = $1")
        .bind(&nickname)
        .fetch_optional(&mut bot.db)
        .await?
        .map(|(s,): (String,)| s)
        .unwrap_or(nickname);

    let query =
        sqlx::query("SELECT alias_secondary FROM aliases WHERE alias_primary = $1").bind(&primary);
    let mut aliases = Vec::<String>::new();
    let mut rows = bot.db.fetch(query);
    while let Some(row) = rows.next().await? {
        aliases.push(row.get(0))
    }

    let msg = if aliases.is_empty() {
        format!("{} doesn't have any aliases.", primary)
    } else {
        format!("{} aliases: {}.", primary, aliases.join(", "))
    };

    bot.irc.privmsg(target, msg).await
}

command!("aliases", aliases);
